package in.forsk;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

import com.androidquery.AQuery;

public class MainActivity extends Activity {

	Context context;
	ImageView imageView;
	Button loadBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		context = this;

		imageView = (ImageView) findViewById(R.id.imageView1);
		loadBtn = (Button) findViewById(R.id.button1);

		loadBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
//				new ImageLoaderAsyncTask(context, "http://iiitkota.forsklabs.in/appLogo.png", imageView).execute();

				ProgressDialog pd =  new ProgressDialog(MainActivity.this);
        			pd.setMessage("Loading");
				new AQuery(MainActivity.this).progress(pd).id(imageView).image("http://iiitkota.forsklabs.in/appLogo.png");
				
			}
		});
	}

}
