package in.forsk;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends Activity {

    Context context;
    ImageView imageView;
    Button loadBtn;

    private ResultReceiver mReceiver;
    String path = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        imageView = (ImageView) findViewById(R.id.imageView1);
        loadBtn = (Button) findViewById(R.id.button1);

        if(savedInstanceState != null){

            path =  savedInstanceState.getString("path_key");

            if (!TextUtils.isEmpty(path)) {
                Bitmap bitmap = BitmapFactory.decodeFile(path);
                if (bitmap != null)
                    imageView.setImageBitmap(bitmap);
            }
        }


        loadBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
//				new ImageLoaderAsyncTask(context, "http://iiitkota.forsklabs.in/appLogo.png",
// imageView).execute();

//				new AQuery(context).id(imageView).image("http://iiitkota.forsklabs.in/appLogo
// .png");

				/* Starting Download Service */
                Intent intent = new Intent(context, DownloadService.class);

				/* Send optional extras to Download IntentService */
                intent.putExtra("url", "http://iiitkota.forsklabs.in/appLogo.png");
                intent.putExtra("requestId", 101);

                startService(intent);

            }
        });

        // We need to register broadcast receiver on the start of the activity
        // need to be unregister at end of the lifecycle of the activity

        mReceiver = new ResultReceiver();

        // To receive the broadcast from teh system, we need define which
        // broadcast android need to send our app
        IntentFilter filter = new IntentFilter("in.forsk.update");
        registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // We need to register broadcast receiver on the start of the activity
        // need to be unregister at end of the lifecycle of the activity
        unregisterReceiver(mReceiver);
    }


    //This has been created as inner class so that within the class we can access button member
    // variable
    //This will run on the UI main thread, so accessing the UI component becomes easy
    public class ResultReceiver extends BroadcastReceiver {

        //this method is called every time, when our app receiver a broadcast for intent filter
        // (in.forsk.update)
        @Override
        public void onReceive(Context context, Intent intent) {

            //Parceable concept is use to exchange the data between the class(through intent)
            int resultCode = intent.getIntExtra("resultCode", -1);
            Bundle resultData = intent.getExtras();

            switch (resultCode) {
                case DownloadService.STATUS_RUNNING:

                    //You can start a progress bar here

                    break;
                case DownloadService.STATUS_FINISHED:
                /* Hide progress bar & extract result from bundle */
//				setProgressBarIndeterminateVisibility(false);

                    path = resultData.getString("result");

                    Bitmap bitmap = BitmapFactory.decodeFile(path);

                    imageView.setImageBitmap(bitmap);

                    break;
                case DownloadService.STATUS_ERROR:
				/* Handle the error */
                    String error = resultData.getString(Intent.EXTRA_TEXT);
                    break;
            }
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        outState.putString("path_key", path);

        super.onSaveInstanceState(outState);
    }
}
